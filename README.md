# Metal Map

| Continent | Country        |                  | Band                    |
|:----------|:---------------|:-----------------|:------------------------|
|           |                |                  | Tengger Cavalry         |
|           | Russia         |                  | Grai                    |
| America   | Canada         | Ontario          | Kittie                  |
| America   | Canada         | Ontario          | Three Days Grace        |
| America   | USA            | Arkansas         | Evanescence             |
| America   | USA            | California       | Korn                    |
| America   | USA            | California       | Linkin Park             |
| America   | USA            | California       | Prophets of Rage        |
| America   | USA            | California       | Steel Panther           |
| America   | USA            | California       | System of a Down        |
| America   | USA            | Florida          | Marilyn Manson          |
| America   | USA            | Georgia          | Combichrist             |
| America   | USA            | Illinois         | Disturbed               |
| America   | USA            | Iowa             | Slipknot                |
| America   | USA            | Iowa             | Stone Sour              |
| America   | USA            | Massachusetts    | Rob Zombie              |
| America   | USA            | Nevada           | Five Finger Death Punch |
| America   | USA            | New York City    | Emigrate                |
| America   | USA            | New York City    | The Pretty Reckless     |
| America   | USA            | Ohio             | Mushroomhead            |
| America   | USA            | Pennsylvania     | Halestorm               |
| America   | USA            | Washington       | Foo Fighters            |
| Asia      | India          |                  | Bloodywood              |
| Asia      | Indonesia      |                  | Voice of Bacetrop       |
| Asia      | Japan          |                  | Maximum The Hormone     |
| Asia      | Japan          | Kanagawa         | Silence iz Mine         |
| Asia      | Mongolia       |                  | The Hu                  |
| Europe    | Austria        |                  | Kontrust                |
| Europe    | Belgium        |                  | Brides of Lucifer       |
| Europe    | Faroe Islands  |                  | Týr                     |
| Europe    | Finland        |                  | Apocalyptica            |
| Europe    | Finland        |                  | Korpiklaani             |
| Europe    | Finland        |                  | Nightwish               |
| Europe    | France         |                  | Skáld                   |
| Europe    | France         | Landes           | Gojira                  |
| Europe    | France         | Loire-Atlantique | Ultra Vomit             |
| Europe    | France         | Paris            | Mass Hysteria           |
| Europe    | France         | Paris            | Shaârghot               |
| Europe    | France         | Seine-et-Marne   | Pleymo                  |
| Europe    | France         | Seine-et-Marne   | Pogo Car Crash Control  |
| Europe    | France         | Yvelines         | Enhancer                |
| Europe    | Germany        |                  | Lord Of The Lost        |
| Europe    | Germany        |                  | Powerwolf               |
| Europe    | Germany        |                  | Rammstein               |
| Europe    | Germany        |                  | Storm Seeker            |
| Europe    | Germany        |                  | Van Canto               |
| Europe    | Italy          |                  | Melodicka Bros          |
| Europe    | Italy          |                  | Wind Rose               |
| Europe    | Netherlands    |                  | Epica                   |
| Europe    | Norway         |                  | FrogLeap                |
| Europe    | Scotland       |                  | Alestorm                |
| Europe    | Spain          | Catalonia        | Ankor                   |
| Europe    | Sweden         |                  | Amaranthe               |
| Europe    | Sweden         |                  | Arch Enemy              |
| Europe    | Sweden         |                  | Avatar                  |
| Europe    | Sweden         |                  | In Flames               |
| Europe    | Sweden         |                  | Meshuggah               |
| Europe    | Sweden         |                  | Sabaton                 |
| Europe    | Switzerland    |                  | Eluveitie               |
| Europe    | Ukraine        |                  | Jinjer                  |
| Europe    | United Kingdom | Wales            | Bullet For My Valentine |
| Europe    | United Kingdom | Wales            | Skindred                |
| Oceania   | Australia      |                  | Parkway Drive           |
| Oceania   | New Zealand    |                  | Alien Weaponry          |
